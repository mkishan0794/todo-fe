import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';

export interface ITask{
  id          : string;
  title       : string;
  description : string;
  dueDate     : string;
}

@Injectable()
export class TasksService {
  
  private _taskListUrl:string = "http://localhost:3000/todo/list/";
    
  constructor(private http:HttpClient) { }

  getTasks(): Observable<ITask[]>{
    return this.http.get<ITask[]>(this._taskListUrl)
  }
}
