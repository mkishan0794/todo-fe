import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { FormsModule, NgForm } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  apiRoot   : string = "http://0.0.0.0:3000";  

  model  = {
    filter      : "",
    title       : "",
    isThisWeek  : "",
    isNextWeek  : "",
    isToday     : "",
    isOverDue   : ""
  }

  constructor(private httpClient:HttpClient) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm, e) {
    let headers = new HttpHeaders();  
    headers = headers.set('Content-Type', 'application/json');
    let url = `${this.apiRoot}/todo/search/`;
    
    var getTitle        = f.value.title;
    var getFilter       = f.value.filter;
    
    // console.log(getFilter)

    switch (getFilter) {
      case "": {
        var thisWeek  = false;
        var nextWeek  = false;
        var today     = false;
        var overDue   = false;
        break;
     }

      case "thisWeek": {
        var thisWeek  = true;
        var nextWeek  = false;
        var today     = false;
        var overDue   = false;
        break;
     }

      case "nextWeek": {
        var thisWeek  = false;
        var nextWeek  = true;
        var today     = false;
        var overDue   = false;
        break;
      }
      
      case "today": {
        var thisWeek  = false;
        var nextWeek  = false;
        var today     = true;
        var overDue   = false;
        break;
      }
      
      case "overDue": {
        var thisWeek  = false;
        var nextWeek  = false;
        var today     = false;
        var overDue   = true;
        break;
      }
    }
  
    this.httpClient.post(url,
      {
        searchQuery : getTitle,
        isThisWeek  : thisWeek,
        isNextWeek  : nextWeek,
        isToday     : today,
        isOverDue   : overDue
      },
      {
        headers: headers
      })

    .subscribe(data => {
      console.log(data);
    }) 
  };
}
