import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { 
    MatButtonModule, 
    MatToolbarModule, 
    MatFormFieldModule,
    MatIconModule,
    MatTabGroup,
    MatTab,
    MatTabsModule,
    MatCardModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatCheckboxModule,
    MatRadioModule
    
    } from '@angular/material';

@NgModule({
    imports: [
        MatButtonModule, 
        MatToolbarModule, 
        MatFormFieldModule,
        MatIconModule,
        MatTabsModule,
        MatCardModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCheckboxModule,
        MatRadioModule
    ],
    exports: [
        MatButtonModule, 
        MatToolbarModule, 
        MatFormFieldModule,
        MatIconModule,
        MatTabsModule,
        MatCardModule,
        MatInputModule,
        MatDatepickerModule,
        MatSelectModule,
        MatNativeDateModule,
        MatCheckboxModule,
        MatRadioModule
    ],
})
export class MaterialModule { }