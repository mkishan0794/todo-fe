import { Component, OnInit } from '@angular/core';

import { TasksService } from './../tasks.service';

@Component({
  selector: 'app-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.css']
})
export class ListTaskComponent implements OnInit {

  public tasks = [];

  constructor(private _taskService:TasksService) { }

  ngOnInit() {
    this._taskService.getTasks()
        .subscribe((data) => {
           this.tasks = data.data;
          // console.log(this.tasks);
        });
        
  };
}
