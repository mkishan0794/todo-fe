import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';;

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { SearchComponent } from './search/search.component';
import { NewTaskComponent } from './new-task/new-task.component';
import { ListTaskComponent } from './list-task/list-task.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { TasksService } from './tasks.service';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    NewTaskComponent,
    ListTaskComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    MatSnackBarModule
  ],
  providers: [TasksService],
  bootstrap: [AppComponent]
})
export class AppModule { }
