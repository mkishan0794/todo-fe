import { Component, OnInit } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { FormsModule, NgForm } from '@angular/forms';

import * as moment from 'moment';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.css']
})
export class NewTaskComponent implements OnInit {
  
  setDuedate: string;
  apiRoot   : string = "http://0.0.0.0:3000";  

  model  = {
    title       : "",
    description : "",
    dueDate     : ""
  }


  constructor(private httpClient:HttpClient) { }

  ngOnInit() {
  }
  onSubmit(f: NgForm) {
    let headers = new HttpHeaders();  
    headers = headers.set('Content-Type', 'application/json');
    let url = `${this.apiRoot}/todo/create/`;
    

    
    
    var getTitle        = f.value.title;
    var getDescription  = f.value.description;
    var setDuedate      = moment(f.value.dueDate);
    
    // console.log(f.value.dueDate);
    // console.log(setDuedate.format("YYYY-MM-DD"));

    
    this.httpClient.post(url,
      {
        title       : getTitle,
        description : getDescription,
        dueDate     : setDuedate.format("YYYY-MM-DD"),
        subTasks    : [
          {
            "title"			:	"Sub Title Test",
            "description"	:	"Sub Description 1"
          }
          ]
      },
      {
        headers: headers
      })

    .subscribe(data => {
      console.log(data);
    }) 
  };

}
